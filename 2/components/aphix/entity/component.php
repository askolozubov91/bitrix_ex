<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
    die();
}

use Bitrix\Main\Entity;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Aphix\Testing\TestTable;
use Aphix\Testing\QuestionTable;

if (!Loader::includeModule($arParams['MODULE_NAME'])) {
    echo Loc::getMessage('APHIX_ENTITY_MODULE_INCLUDE_FAILED');
    
    return;
}

$arDefaultUrlTemplatesSEF = array(
    'list'   => '',
    'create' => 'create/',
    'edit'   => '#ID#/',
);
$arDefaultVariableAliasesSEF = array();
$arComponentVariables = array(
    'ID',
);
$SEF_FOLDER = $arParams['SEF_FOLDER'];
$arUrlTemplates = array();
$arVariables = array();

if (is_array($arParams['COMPONENT_VARIABLES']) && count($arParams['COMPONENT_VARIABLES']) > 0)
{
	$arComponentVariables = array_merge($arComponentVariables, $arParams['COMPONENT_VARIABLES']);
}

$arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplatesSEF, $arParams["SEF_URL_TEMPLATES"]);
$arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliasesSEF, $arParams["VARIABLE_ALIASES"]);

$componentPage = CComponentEngine::ParseComponentPath(
    $SEF_FOLDER,
    $arUrlTemplates,
    $arVariables
);

if (strlen($componentPage) <= 0)
{
    $componentPage = 'list';
}

CComponentEngine::InitComponentVariables(
    $componentPage, 
    $arComponentVariables, 
    $arVariableAliases, 
    $arVariables
);

foreach ($arUrlTemplates as $i => $arUrlTemplate)
{
    $arUrlTemplates[$i] = $SEF_FOLDER.$arUrlTemplate;
}

$arResult = array(
    'FOLDER'        => $SEF_FOLDER,
    'URL_TEMPLATES' => $arUrlTemplates,
    'VARIABLES'     => $arVariables,
    'ALIASES'       => $arVariableAliases,
);

$arResult['ENTITY_CLASS'] = $arParams['ENTITY_CLASS'];
$arResult['LIST_FIELDS'] = $arParams['LIST_FIELDS'];
$arResult['EDITABLE_FIELDS'] = $arParams['EDITABLE_FIELDS'];

$arResult['CREATE_LINK'] = CComponentEngine::MakePathFromTemplate(
    $arUrlTemplates['create'],
    $arVariables
);
$arResult['CREATE_LINK_TITLE'] = isset($arParams['CREATE_LINK_TITLE']) ? $arParams['CREATE_LINK_TITLE'] : Loc::getMessage("APHIX_ENTITY_CREATE_LINK_TITLE");

$arResult['BACK_URL'] = CComponentEngine::MakePathFromTemplate(
    $arUrlTemplates['list'],
    $arVariables
);

if (in_array($componentPage, array('list', 'create')))
{
	$arResult['PAGE_TITLE'] = isset($arParams['PAGE_TITLES'][$componentPage]) ? $arParams['PAGE_TITLES'][$componentPage] : Loc::getMessage("APHIX_ENTITY_{$componentPage}_PAGE_TITLE");
}

$this->IncludeComponentTemplate($componentPage);
?>