<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
    die();
}

use Bitrix\Main\Localization\Loc;
?>
<div style="margin-bottom: 20px;">
    <a href="<?php echo $arResult['CREATE_LINK']; ?>"><?php echo $arResult['CREATE_LINK_TITLE']; ?></a>
</div>
<?php
$APPLICATION->IncludeComponent(
    'aphix:entity.list',
    '',
    array(
        'ENTITY_CLASS' => $arResult['ENTITY_CLASS'],
        'FIELDS' => $arResult['LIST_FIELDS'],
        'URL_TEMPLATES' => $arResult['URL_TEMPLATES'],
        'VARIABLES' => $arResult['VARIABLES'],
        'PAGE_TITLE' => $arResult['PAGE_TITLE'],
    ),
    $component
);
?>