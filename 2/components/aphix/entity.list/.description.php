<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
    die();
}

use Bitrix\Main\Localization\Loc;

$arComponentDescription = array(
	'NAME'        => Loc::getMessage('APHIX_ENTITY_LIST_COMPONENT_NAME'),
	'DESCRIPTION' => Loc::getMessage('APHIX_ENTITY_LIST_COMPONENT_DESCRIPTION'),
);

?>