<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
    die();
}
?>
<?php if ($arResult['OBJECTS']): ?>
    <table border="1" cellpadding="5">
        <thead>
            <tr>
                <?php foreach ($arResult['FIELDS'] as $fieldLabel): ?>
                    <th><?php echo $fieldLabel; ?></th>
                <?php endforeach; ?>
                <?php foreach ($arResult['ACTIONS'] as $actionLabel): ?>
                    <th><?php echo $actionLabel; ?></th>
                <?php endforeach; ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($arResult['OBJECTS'] as $arItem): ?>
                <tr>
                    <?php foreach ($arResult['FIELDS'] as $fieldName => $fieldLabel): ?>
                        <td><?php echo $arItem[$fieldName]; ?></td>
                    <?php endforeach; ?>
                    <?php foreach ($arResult['ACTIONS'] as $actionName => $arActionParams): ?>
                        <td><a href="<?php echo $arItem['ACTIONS_LINKS'][$actionName]; ?>"><?php echo $arActionParams['LABEL']; ?></a></td>
                    <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>