<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
    die();
}

use Bitrix\Main\Localization\Loc;

$entityClass = isset($arParams['ENTITY_CLASS']) ? $arParams['ENTITY_CLASS'] : '';
$filter = isset($arParams['FILTER']) ? $arParams['FILTER'] : array();

$arResult['OBJECTS'] = array();

$select = array();

foreach ($arParams['FIELDS'] as $fieldName => $fieldLabel)
{
    $select[] = $fieldName;
}

$result = $entityClass::getList(
    array(
        'select' => $select,
        'filter' => $filter,
        'limit' => 10,
    )
);

$arResult['ACTIONS'] = array(
    'EDIT' => array(
        'URL_TEMPLATE' => $arParams['URL_TEMPLATES']['edit'],
        'LABEL' => Loc::getMessage("APHIX_ENTITY_LIST_EDIT_ACTION_TITLE"),
    ),
    'DELETE' => array(
        'URL_TEMPLATE' => $arParams['URL_TEMPLATES']['edit'],
        'LABEL' => Loc::getMessage("APHIX_ENTITY_LIST_DELETE_ACTION_TITLE"),
        'QUERY_PARAMS' => array(
            'action' => 'delete',
        ),
    ),
);

while ($arItem = $result->fetch())
{
    foreach ($arResult['ACTIONS'] as $actionName => $arActionParams)
    {
        $variables = array_merge($arParams['VARIABLES'], array('ID' => $arItem['ID']));

        $arItem['ACTIONS_LINKS'][$actionName] = CComponentEngine::MakePathFromTemplate(
            $arActionParams['URL_TEMPLATE'],
            $variables
        );
        
        if (isset($arActionParams['QUERY_PARAMS']))
        {
            $actionLink = new \Bitrix\Main\Web\Uri($arItem['ACTIONS_LINKS'][$actionName]);
            $arItem['ACTIONS_LINKS'][$actionName] = $actionLink->addParams($arActionParams['QUERY_PARAMS'])->getUri();
        }
    }
    
    $arResult['OBJECTS'][] = $arItem;
}

foreach ($arParams['FIELDS'] as $fieldKey => $fieldLabel)
{
    if (false === $fieldLabel)
    {
        $label = '';
    }
    elseif ($fieldLabel == '')
    {
        $label = $fieldKey;
    }
    else {
        $label = $fieldLabel;
    }
    
    $arResult['FIELDS'][$fieldKey] = $label;
}

if (false) // TODO: работа с кешем 
{

} 
else 
{ 
    $this->IncludeComponentTemplate(); 
}

$APPLICATION->SetTitle($arParams['PAGE_TITLE']);
?>