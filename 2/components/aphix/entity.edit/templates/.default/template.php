<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
    die();
}

use Bitrix\Main\Localization\Loc;
?>
<div class="object-edit-form">
    <?php if ($arResult['NOTIFIES']): ?>
        <div class="notifies">
            <?php foreach ($arResult['NOTIFIES'] as $arNotify): ?>
                <?php if ($arNotify['TYPE'] == 'SUCCESS'): ?>
                    <div class="success">
                        <div class="text"><?php echo $arNotify['TEXT']; ?></div>
                    </div>
                <?php  elseif ($arNotify['TYPE'] == 'ERROR'): ?>
                    <div class="error">
                        <div class="text"><?php echo $arNotify['TEXT']; ?></div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <form action="<?php echo $arResult['FORM']['ACTION']; ?>" method="post">
        <input type="hidden" name="action" value="save">
        <?php foreach ($arResult['NONEDITABLE_FIELDS'] as $fieldName => $fieldValue): ?>
            <input type="hidden" name="<?php echo $fieldName; ?>" value="<?php echo $fieldValue; ?>">
        <?php endforeach; ?>
        <?php foreach ($arResult['EDITABLE_FIELDS'] as $editableField): ?>
            <div class="form-item<?php if (isset($arResult['FORM']['ERRORS'][$editableField['NAME']])) { echo ' invalid'; } ?>">
                <label><?php echo $editableField['LABEL']; ?></label>
                <?php if (isset($arResult['FORM']['ERRORS'][$editableField['NAME']])): ?>
                    <div class="error"><?php echo $arResult['FORM']['ERRORS'][$editableField['NAME']]; ?></div>
                <?php endif; ?>
                <?php if ($editableField['DATA_TYPE'] == 'integer' || $editableField['DATA_TYPE'] == 'string'): ?>
                    <input type="text" name="<?php echo $editableField['NAME']; ?>" value="<?php echo $arResult['OBJECT'][$editableField['NAME']]; ?>">
                <?php elseif ($editableField['DATA_TYPE'] == 'text'): ?>
                    <textarea name="<?php echo $editableField['NAME']; ?>"><?php echo $arResult['OBJECT'][$editableField['NAME']]; ?></textarea>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
        <?php if (isset($arResult['OBJECT'])): ?>
            <button class="action-btn save-btn" type="submit" name="return" value="Y"><?php echo Loc::getMessage('APHIX_ENTITY_EDIT_SAVE_BUTTON_TITLE'); ?></button>
            <button class="action-btn apply-btn" type="submit"><?php echo Loc::getMessage('APHIX_ENTITY_EDIT_APPLY_BUTTON_TITLE'); ?></button>
            <a class="action-btn delete-btn" href="<?php echo $arResult['FORM']['DELETE_LINK']; ?>"><?php echo Loc::getMessage('APHIX_ENTITY_EDIT_DELETE_LINK_TITLE'); ?></a>
        <?php else: ?>
            <button class="action-btn apply-btn" type="submit" name="go_to_edit" value="Y"><?php echo Loc::getMessage('APHIX_ENTITY_EDIT_CREATE_BUTTON_TITLE'); ?></button>
            <button class="action-btn save-btn" type="submit"><?php echo Loc::getMessage('APHIX_ENTITY_EDIT_CREATE_AND_NEW_BUTTON_TITLE'); ?></button>
            <button class="action-btn save-btn" type="submit" name="return" value="Y"><?php echo Loc::getMessage('APHIX_ENTITY_EDIT_CREATE_AND_RETURN_BUTTON_TITLE'); ?></button>
        <?php endif; ?>
    </form>
</div>