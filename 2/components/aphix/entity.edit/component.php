<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
    die();
}

use Bitrix\Main\Context;
use Bitrix\Main\Localization\Loc;

$entityClass = isset($arParams['ENTITY_CLASS']) ? $arParams['ENTITY_CLASS'] : '';
$entityFields = $entityClass::getEntity()->getFields();
$request = Context::getCurrent()->getRequest();

/*
 * Save action
 */

$action = $request->get('action');

if ($action == 'save')
{    
    $objectData = array();
    
    foreach ($arParams['EDITABLE_FIELDS'] as $fieldName => $fieldLabel)
    {
        if (null !== ($fieldValue = $request->getPost($fieldName)))
        {
            $objectData[$fieldName] = $fieldValue;
        }
    }

    if ($arParams['NONEDITABLE_FIELDS'])
    {
        $objectData = array_merge($objectData, $arParams['NONEDITABLE_FIELDS']);
    }
    
    if (count($objectData) > 0)
    {
        if ($arParams['VARIABLES']['ID'])
        {
            $result = $entityClass::update($arParams['VARIABLES']['ID'], $objectData);
        }
        else
        {
            $result = $entityClass::add($objectData);
        }
        
        if (!$result->isSuccess())
        {
            $errors = $result->getErrors();
            
            foreach ($errors as $error)
            {
                $arResult['FORM']['ERRORS'][$error->getField()->getName()] = $error->getMessage();
            }

            $arResult['NOTIFIES'][] = array(
                'TYPE' => 'ERROR',
                'TEXT' => Loc::getMessage('APHIX_ENTITY_EDIT_UPDATE_FAILED'),
            );
        }
        else
        {
            $arResult['NOTIFIES'][] = array(
                'TYPE' => 'SUCCESS',
                'TEXT' => Loc::getMessage('APHIX_ENTITY_EDIT_UPDATE_SUCCESS'),
            );

            if ($request->get('return') == 'Y')
            {
                $backUrl = $request->get('back_url');

                if (!$backUrl)
                {
                    $backUrl = $arParams['BACK_URL'];
                }
                
                if ($backUrl)
                {
                    LocalRedirect($backUrl);
                }
            }
            elseif ($request->get('go_to_edit') == 'Y')
            {
                $objectId = $result->getId();

                $editLink = CComponentEngine::MakePathFromTemplate(
                    $arParams['URL_TEMPLATES']['edit'],
                    array_merge(
                        $arParams['VARIABLES'],
                        array(
                            'ID' => $objectId,
                        )
                    )
                );

                LocalRedirect($editLink);
            }
        }
    }
}

/*
 * Delete action
 */

if ('delete' == $request->get('action'))
{   
    $result = $entityClass::delete($arParams['VARIABLES']['ID']);
        
    if ($result->isSuccess())
    {
        $backUrl = $request->get('back_url');

        if (!$backUrl)
        {
            $backUrl = $arParams['BACK_URL'];
        }

        if ($backUrl)
        {
            LocalRedirect($backUrl);
        }
    }
}

/*
 * Form fields
 */

foreach ($entityFields as $entityField)
{
    if (array_key_exists($entityField->getName(), $arParams['EDITABLE_FIELDS']))
    {
        $fieldName = $entityField->getName();
        
        $arResult['EDITABLE_FIELDS'][] = array(
            'NAME' => $fieldName,
            'DATA_TYPE' => $entityField->getDataType(),
            'LABEL' => ('' == $arParams['EDITABLE_FIELDS'][$fieldName] ? $fieldName : $arParams['EDITABLE_FIELDS'][$fieldName]),
            'EDITABLE' => true,
        );
    }
}

$arResult['NONEDITABLE_FIELDS'] = $arParams['NONEDITABLE_FIELDS'];

/*
 * Form action url
 */

$requestUri = new \Bitrix\Main\Web\Uri(Context::getCurrent()->getServer()->getRequestUri());
$arResult['FORM']['ACTION'] = $requestUri->getUri();

/*
 * If EDIT page -> Form fields values
 */

if ($arParams['VARIABLES']['ID'])
{
    $select = array();

    foreach ($arParams['EDITABLE_FIELDS'] as $fieldName => $fieldLabel)
    {
        $select[] = $fieldName;
    }

    $arResult['OBJECT'] = $entityClass::getList(
        array(
            'select' => $select,
            'filter' => array(
                '=ID' => $arParams['VARIABLES']['ID'],
            ),
            'limit' => 1,
        )
    )->fetch();

    $arResult['FORM']['DELETE_LINK'] = $requestUri->addParams(array('action' => 'delete'))->getUri();
}

if (false) // TODO: работа с кешем 
{

} 
else 
{ 
    $this->IncludeComponentTemplate(); 
}

if ($arParams['VARIABLES']['ID'])
{
    $pageTitle = $arResult['OBJECT'] ? $arResult['OBJECT']['TITLE'] : Loc::getMessage('APHIX_ENTITY_EDIT_OBJECT_NOT_FOUND');

    $APPLICATION->SetTitle($pageTitle);
    $APPLICATION->AddChainItem($pageTitle);
}
else
{
    $APPLICATION->SetTitle($arParams['PAGE_TITLE']);
    $APPLICATION->AddChainItem($arParams['PAGE_TITLE']);
}

?>