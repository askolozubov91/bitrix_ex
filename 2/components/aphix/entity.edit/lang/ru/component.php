<?php
$MESS['APHIX_ENTITY_EDIT_UPDATE_FAILED'] = 'При сохранении возникли ошибки';
$MESS['APHIX_ENTITY_EDIT_UPDATE_SUCCESS'] = 'Сохранение прошло успешно';
$MESS['APHIX_ENTITY_EDIT_OBJECT_NOT_FOUND'] = 'Запрашиваемый объект не найден';
?>