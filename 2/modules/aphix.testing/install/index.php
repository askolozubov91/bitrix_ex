<?php

use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;

class aphix_testing extends CModule
{
    public $MODULE_ID = "aphix.testing";
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $PARTNER_NAME;
    public $PARNTER_URL;

    function aphix_testing()
    {
        $arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path."/version.php");
        
        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
        {
            $this->MODULE_VERSION      = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
        
        $this->MODULE_NAME        = Loc::getMessage("APHIX_TESTING_MODULE_NAME");
        $this->MODULE_DESCRIPTION = Loc::getMessage("APHIX_TESTING_MODULE_DESCRIPTION");
        $this->PARTNER_NAME       = Loc::getMessage("APHIX_TESTING_PARTNER_NAME");
        $this->PARTNER_URL        = Loc::getMessage("APHIX_TESTING_PARTNER_URL");
    }

    function DoInstall()
    {
        global $APPLICATION;

        RegisterModule($this->MODULE_ID);
        
        $APPLICATION->IncludeAdminFile(Loc::getMessage("APHIX_TESTING_INSTALL_TITLE"), Application::getDocumentRoot()."/local/modules/aphix.testing/install/step.php");
        
        return true;
    }

    function DoUninstall()
    {
        global $APPLICATION;
        
        UnRegisterModule($this->MODULE_ID);
        
        $APPLICATION->IncludeAdminFile(Loc::getMessage("APHIX_TESTING_UNINSTALL_TITLE"), Application::getDocumentRoot()."/local/modules/aphix.testing/install/unstep.php");
        
        return true;
    }
}