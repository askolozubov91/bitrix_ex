<?php
if (!check_bitrix_sessid())
{
    return;
}

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
?>
<?php echo CAdminMessage::ShowNote(Loc::getMessage("APHIX_TESTING_INSTALL_SUCCESS")); ?>
<form action="<?php echo $APPLICATION->GetCurPage(); ?>">
	<input type="hidden" name="lang" value="<?php echo LANG; ?>">
	<input type="submit" name="" value="<?php echo Loc::getMessage("MOD_BACK"); ?>">
<form>