<?php
$MESS['APHIX_TESTING_MODULE_NAME'] = 'APHIX Тестирование';
$MESS['APHIX_TESTING_MODULE_DESCRIPTION'] = 'Описание модуля "APHIX Тестирование"';
$MESS['APHIX_TESTING_INSTALL_TITLE'] = 'Установка модуля "APHIX Тестирование"';
$MESS['APHIX_TESTING_UNINSTALL_TITLE'] = 'Деинсталляция модуля "APHIX Тестирование"';
$MESS['APHIX_TESTING_PARTNER_NAME'] = 'APHIX';
$MESS['APHIX_TESTING_PARTNER_URI'] = 'http://www.aphix.com';
?>