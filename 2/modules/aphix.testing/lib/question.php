<?php
namespace Aphix\Testing;

use Bitrix\Main\Entity;

class QuestionTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'aphix_questions';
    }
    
    public static function getMap()
    {
        return array(
            new Entity\IntegerField(
                'ID', 
                array(
                    'primary' => true,
                    'autocomplete' => true,
                )
            ),
            new Entity\StringField(
                'TITLE',
                array(
                    'required' => true,
                    'validation' => function() {
                        return array(
                            new Entity\Validator\Length(null, 256),
                        );
                    },
                )
            ),
            new Entity\StringField(
                'TYPE',
                array(
                    'required' => true,
                    'validation' => function() {
                        return array(
                            new Entity\Validator\Length(null, 256),
                        );
                    },
                )
            ),
            new Entity\TextField(
                'OPTIONS',
                array(
                    'required' => true,
                )
            ),
            new Entity\TextField(
                'CORRECT_ANSWER',
                array(
                    'required' => true,
                )
            ),
            new Entity\IntegerField('TEST_ID'),
            new Entity\ReferenceField(
                'Test',
                Test::class,
                array(
                    '=this.TEST_ID' => 'ref.ID',
                )
            )
        );
    }

    public static function deleteByTestId($testId = 0)
    {
        $testId = intval($testId);

        $connection = self::getEntity()->getConnection();
        $helper = $connection->getSqlHelper();
        $tableName = self::getTableName();
        $where = $helper->prepareAssignment($tableName, 'TEST_ID', $testId);
        $sql = "DELETE FROM ".$tableName." WHERE ".$where;
        $result = $connection->query($sql);

        return $result;
    }

}
?>