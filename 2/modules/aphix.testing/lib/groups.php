<?php
namespace Aphix\Testing;

use Bitrix\Main\Entity;

class GroupTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'aphix_groups';
    }
    
    public static function getMap()
    {
        return array(
            new Entity\IntegerField(
                'ID',
                array(
                    'primary' => true,
                    'autocomplete' => true,
                )
            ),
            new Entity\StringField(
                'TITLE',
                array(
                    'required' => 'true',
                    'validation' => function() {
                        return array(
                            new Entity\Validator\Length(null, 256),
                        );
                    },
                )
            ),
            new Entity\TextField('DESCRIPTION'),
        );
    }
}
?>