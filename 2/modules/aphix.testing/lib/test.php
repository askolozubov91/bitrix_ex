<?php
namespace Aphix\Testing;

use Bitrix\Main\Entity;

class TestTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'aphix_tests';
    }
    
    public static function getMap()
    {
        return array(
            new Entity\IntegerField(
                'ID',
                array(
                    'primary' => true,
                    'autocomplete' => true,
                )
            ),
            new Entity\StringField(
                'TITLE',
                array(
                    'required' => 'true',
                    'validation' => function() {
                        return array(
                            new Entity\Validator\Length(null, 256),
                        );
                    },
                )
            ),
            new Entity\TextField('DESCRIPTION'),
        );
    }

    public static function onDelete(Entity\Event $event)
    {
        $result = new Entity\EventResult;
        $primary = $event->getParameter('primary');
        
        QuestionTable::deleteByTestId($primary['ID']);

        return $result;
    }
}
?>