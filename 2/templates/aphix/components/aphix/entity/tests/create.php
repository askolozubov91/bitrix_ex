<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
    die();
}

use Bitrix\Main\Localization\Loc;

$APPLICATION->IncludeComponent(
    'aphix:entity.edit',
    '',
    array(
        'URL_TEMPLATES' => $arResult['URL_TEMPLATES'],
        'VARIABLES' => $arResult['VARIABLES'],
        'ENTITY_CLASS' => $arResult['ENTITY_CLASS'],
        'EDITABLE_FIELDS' => $arResult['EDITABLE_FIELDS'],
        'PAGE_TITLE' => $arResult['PAGE_TITLE'],
        'CREATE_LINK' => $arResult['CREATE_LINK'],
        'BACK_URL' => $arResult['BACK_URL'],
    ),
    $component
);
?>