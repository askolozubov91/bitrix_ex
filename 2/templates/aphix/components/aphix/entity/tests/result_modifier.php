<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
    die();
}

use \Bitrix\Main\Entity;
use \Aphix\Testing\QuestionTable;

if ($this->getComponent()->GetTemplatePage() == 'edit')
{
    $arResult['QUESTIONS_LIST_LINK'] = CComponentEngine::MakePathFromTemplate(
        $arResult['URL_TEMPLATES']['edit'],
        array(
            'ID' => $arResult['VARIABLES']['ID'],
        )
    ).'questions/';

    $arTestQuestions = QuestionTable::getList(
        array(
            'select' => array(
                new Entity\ExpressionField('QUESTIONS_COUNT', 'COUNT(*)'),
            ),
            'filter' => array(
                '=TEST_ID' => $arResult['VARIABLES']['ID'],
            ),
        )
    )->fetch();
    
    $arResult['QUESTIONS_COUNT'] = $arTestQuestions['QUESTIONS_COUNT'];
}
?>