<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
    die();
}

use Bitrix\Main\Localization\Loc;

$APPLICATION->IncludeComponent(
    'aphix:entity.edit',
    '',
    array(
        'ENTITY_CLASS' => $arResult['ENTITY_CLASS'],
        'EDITABLE_FIELDS' => $arResult['EDITABLE_FIELDS'],
        'NONEDITABLE_FIELDS' => array(
            'TEST_ID' => $arResult['TEST']['ID'],
        ),
        'URL_TEMPLATES' => $arResult['URL_TEMPLATES'],
        'VARIABLES' => $arResult['VARIABLES'],
        'BACK_URL' => $arResult['BACK_URL'],
    ),
    $component
);
?>