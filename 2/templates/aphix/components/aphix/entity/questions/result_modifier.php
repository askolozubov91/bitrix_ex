<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
    die();
}

use \Bitrix\Main\Localization\Loc;
use \Aphix\Testing\TestTable;

$arResult['TEST'] = TestTable::getList(
    array(
        'select' => array(
            'ID',    
            'TITLE',    
        ),
        'filter' => array(
            '=ID' => $arResult['VARIABLES']['TEST_ID'],
        ),
        'limit' => 1,
    )
)->fetch();

$arResult['TEST']['LINK'] = CComponentEngine::MakePathFromTemplate(
    $arResult['FOLDER'].'#TEST_ID#/',
    array(
        'TEST_ID' => $arResult['VARIABLES']['TEST_ID'],
    )
);

$arResult['QUESTIONS_LIST'] = array(
    'TITLE' => Loc::getMessage('APHIX_TESTS_QUESTIONS_SECTION_TITLE'),
    'LINK' => CComponentEngine::MakePathFromTemplate(
        $arResult['URL_TEMPLATES']['list'],
        array(
            'TEST_ID' => $arResult['VARIABLES']['TEST_ID'],
        )
    ),
);

$APPLICATION->AddChainItem($arResult['TEST']['TITLE'], $arResult['TEST']['LINK']);
$APPLICATION->AddChainItem($arResult['QUESTIONS_LIST']['TITLE'], $arResult['QUESTIONS_LIST']['LINK']);
?>