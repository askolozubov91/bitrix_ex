<?php
if( !defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
    die();
}
?>
<!DOCTYPE html>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html;charset=<?php echo SITE_CHARSET; ?>"/>
    <link href="<?php echo CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH.'/template_styles.css')?>" type="text/css" rel="stylesheet">
    <?php CJSCore::Init(); ?>
    <?php $APPLICATION->ShowCSS(true, true); ?>
    <?php $APPLICATION->ShowHeadStrings(); ?>
    <?php $APPLICATION->ShowHeadScripts(); ?>
    <title><?php $APPLICATION->ShowTitle()?></title>
    </head>
    <body>
        <?php $APPLICATION->ShowPanel(); ?>
        <?php
        $context = \Bitrix\Main\Application::getInstance()->getContext();
        $server = $context->getServer();
        var_dump($server->get("REAL_FILE_PATH"));
        var_dump($server->get("SCRIPT_NAME"));
        ?>
        <div class="wrapper">
            <div class="sidebar">
                <?php
                $APPLICATION->IncludeComponent(
                    'bitrix:menu',
                    'tree',
                    Array(
                         'ROOT_MENU_TYPE' => 'account', 
                         'MAX_LEVEL' => '3', 
                         'CHILD_MENU_TYPE' => 'account_sub',
                         'USE_EXT' => 'Y',
                         'CACHE_TYPE' => 'N'
                     )
                );
                ?>
            </div>
            <div class="content">
                <?php
                $APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
                        "START_FROM" => "1",
                        "PATH" => "",
                        "SITE_ID" => "-"
                    ),
                    false,
                    Array('HIDE_ICONS' => 'Y')
                );
                ?>
                <h1><?php $APPLICATION->ShowTitle()?></h1>