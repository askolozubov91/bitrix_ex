<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class LicensesHelper
{
    private $tempDataFile = __DIR__ . '/temp_data.txt';
    private $licensesTypesHLBId = HLB_CATALOGUE_LICENSES_TYPES;
    private $licensesProductsHLBId = HLB_CATALOGUE_LICENSES_PRODUCTS;
    private $licensesListHLBId = HLB_CATALOGUE_LICENSES_LIST;
    private $stepSize = 3;

    function __construct()
    {
        CModule::IncludeModule('highloadblock');
    }

    function clearPreviousData()
    {
        \Bitrix\Main\IO\File::putFileContents(
            $this->tempDataFile,
            ''
        );

        $hlblocks = array($this->licensesTypesHLBId, $this->licensesProductsHLBId, $this->licensesListHLBId);

        foreach ($hlblocks as $hlblock)
        {
            $arHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock)->fetch();
            $obEntity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
            $strEntityDataClass = $obEntity->getDataClass();

            \Bitrix\Main\Application::getConnection()->truncateTable($strEntityDataClass::getTableName());
        }
    }

    function processStep() {
        $tempData = $this->readTempData();
        $step = !empty($tempData['nextStep']) ? $tempData['nextStep'] : 'types';

        switch ($step) {

            case 'types':
                $licensesTypes = $this->parseTypes();

                if ($licensesTypes) {
                    $licensesTypes = $this->saveTypesToDB($licensesTypes);
                }

                $this->writeTempData(
                    'products',
                    array(
                        'licensesTypes' => $licensesTypes,
                    )
                );

                $this->sendSuccessResponse(
                    'Parsing products',
                    5
                );

                break;

            case 'products':
                $licensesTypes = !empty($tempData['data']['licensesTypes']) ? $tempData['data']['licensesTypes'] : null;

                if (!$licensesTypes)
                {
                    $this->sendErrorResponse('$licensesTypes is empty');
                }

                $licensesProducts = $this->parseProducts($licensesTypes);

                if ($licensesProducts) {
                    $licensesProducts = $this->saveProductsToDB($licensesProducts);
                }

                $this->writeTempData(
                    'licenses',
                    array(
                        'licensesProducts' => $licensesProducts,
                        'startIndex' => 0,
                    )
                );

                $this->sendSuccessResponse(
                    'Parsing product\'s licenses (0/' . count($licensesProducts) . ')',
                    10
                );

                break;

            case 'licenses':
                $licensesProducts = !empty($tempData['data']['licensesProducts']) ? $tempData['data']['licensesProducts'] : array();
                $startIndex = isset($tempData['data']['startIndex']) ? (int) $tempData['data']['startIndex'] : null;
                $totalCount = count($licensesProducts);
                $licensesList = array();

                if ($totalCount == 0)
                {
                    $this->sendErrorResponse('$licensesProducts is empty');
                }

                for ($i = $startIndex; $i < $totalCount && $i < $startIndex + $this->stepSize; $i++)
                {
                    $licensesProductId = $licensesProducts[$i]['CODE'];

                    // TYPE = file, MICROSOFT = false
                    $licensesList = array_merge(
                        $licensesList,
                        $this->parseLicenses(
                            "http://partweb.1c.ru/partnersupport/Calc.ashx?act=3&getSQLLic=false&value=$licensesProductId",
                            'file',
                            false,
                            $licensesProducts[$i]['ID']
                        )
                    );

                    // TYPE = server, MICROSOFT = false
                    $licensesList = array_merge(
                        $licensesList,
                        $this->parseLicenses(
                            "http://partweb.1c.ru/partnersupport/Calc.ashx?act=4&getSQLLic=false&value=$licensesProductId",
                            'server',
                            false,
                            $licensesProducts[$i]['ID']
                        )
                    );

                    // TYPE = file, MICROSOFT = true
                    $licensesList = array_merge(
                        $licensesList,
                        $this->parseLicenses(
                            "http://partweb.1c.ru/partnersupport/Calc.ashx?act=3&getSQLLic=true&value=$licensesProductId",
                            'file',
                            true,
                            $licensesProducts[$i]['ID']
                        )
                    );

                    // TYPE = server, MICROSOFT = true
                    $licensesList = array_merge(
                        $licensesList,
                        $this->parseLicenses(
                            "http://partweb.1c.ru/partnersupport/Calc.ashx?act=4&getSQLLic=true&value=$licensesProductId",
                            'server',
                            true,
                            $licensesProducts[$i]['ID']
                        )
                    );

                }

                if ($licensesList) {
                    $this->saveLicensesToDB($licensesList);
                }

                if ($i == $totalCount)
                {
                    \Bitrix\Main\IO\File::deleteFile($this->tempDataFile);

                    $this->sendFinishedResponse('Parsing finished');
                }
                else
                {
                    $this->writeTempData(
                        'licenses',
                        array(
                            'licensesProducts' => $licensesProducts,
                            'startIndex' => $i,
                        )
                    );

                    $this->sendSuccessResponse(
                        'Parsing product\'s licenses (' . $i . '/' . $totalCount . ')',
                        10 + round($i / $totalCount * 90, 2)
                    );
                }

                break;

            default:
        }
    }

    function parseTypes()
    {
        $licensesTypes = array();
        $html = $this->getUrlHtml('http://partweb.1c.ru/partnersupport/Calc.aspx');
        $doc = new DOMDocument();
        $doc->loadHTML($html);
        $typesSelectOptions = $doc->getElementById(mb_convert_encoding('_ctl0_Body_ddl�����������������������', 'utf-8', 'windows-1251'))->getElementsByTagName('option');

        for ($i = 0; $i < $typesSelectOptions->length; $i++)
        {
            $item = $typesSelectOptions->item($i);
            $attrValue = $item->getAttribute('value');

            if ($attrValue !== '0')
            {
                $licensesTypes[$attrValue] = array(
                    'NAME' => $item->textContent,
                );
            }
        }

        return $licensesTypes;
    }

    function saveTypesToDB($licensesTypes)
    {
        if ($licensesTypes)
        {
            $arHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::getById($this->licensesTypesHLBId)->fetch();
            $obEntity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
            $strEntityDataClass = $obEntity->getDataClass();

            foreach ($licensesTypes as $licensesTypeId => $licensesType)
            {
                $arElementFields = array(
                    'UF_NAME' => mb_convert_encoding($licensesType['NAME'], 'windows-1251', 'utf-8'),
                );
                $obResult = $strEntityDataClass::add($arElementFields);

                if ($obResult->isSuccess())
                {
                    $licensesTypes[$licensesTypeId]['ID'] = $obResult->getID();
                }
            }
        }

        return $licensesTypes;
    }

    function parseProducts($licensesTypes)
    {
        $licensesProducts = array();
        $doc = new DOMDocument();

        foreach ($licensesTypes as $licensesTypeId => $licensesType)
        {
            $html = mb_convert_encoding($this->getUrlHtml("http://partweb.1c.ru/partnersupport/Calc.ashx?act=1&value=$licensesTypeId"), 'html-entities', "utf-8");
            $doc->loadHTML($html);
            $prodsSelectOptions = $doc->getElementsByTagName('option');

            for ($i = 0; $i < $prodsSelectOptions->length; $i++)
            {
                $item = $prodsSelectOptions->item($i);
                $attrValue = $item->getAttribute('value');

                if ($attrValue === '0')
                {
                    continue;
                }

                $html = $this->getUrlHtml("http://partweb.1c.ru/partnersupport/Calc.ashx?act=6&value=$attrValue");
                $price = substr($html, strpos($html, 'CDATA[') + 6);
                $price = substr($price, 0, strpos($price, ']]>'));
                preg_match_all('!\d+|,!', $price, $price);
                $price = floatval(implode('', $price[0]));

                $licensesProducts[] = array(
                    'CODE' => $attrValue,
                    'NAME' => $item->textContent,
                    'PRICE' => $price,
                    'LICENSE_TYPE' => $licensesType['ID'],
                );
            }
        }

        return $licensesProducts;
    }

    function saveProductsToDB($licensesProducts) {
        if ($licensesProducts)
        {
            $arHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::getById($this->licensesProductsHLBId)->fetch();
            $obEntity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
            $strEntityDataClass = $obEntity->getDataClass();

            foreach ($licensesProducts as $i => $licensesProduct)
            {
                $arElementFields = array(
                    'UF_NAME' => mb_convert_encoding($licensesProduct['NAME'], 'windows-1251', 'utf-8'),
                    'UF_PRICE' => $licensesProduct['PRICE'],
                    'UF_LICENSE_TYPE' => $licensesProduct['LICENSE_TYPE'],
                );
                $obResult = $strEntityDataClass::add($arElementFields);

                if ($obResult->isSuccess())
                {
                    $licensesProducts[$i]['ID'] = $obResult->getID();
                }
            }
        }

        return $licensesProducts;
    }

    function parseLicenses($url, $type, $microsoft, $licensesProductDBId)
    {
        $licensesList = array();
        $doc = new DOMDocument();
        $html = mb_convert_encoding($this->getUrlHtml($url), 'html-entities', "utf-8");
        $doc->loadHTML($html);
        $licensesRows = $doc->getElementsByTagName('tr');

        for ($i = 0; $i < $licensesRows->length; $i++)
        {
            $licenseCols = $licensesRows->item($i)->getElementsByTagName('td');

            if ($licenseCols->item(0)->textContent === null)
            {
                continue;
            }

            $usersCount = null;
            preg_match('/�������� �� (\d+) �����/', mb_convert_encoding($licenseCols->item(1)->textContent, 'windows-1251', 'utf-8'), $usersCount);

            $licensesList[$licenseCols->item(0)->textContent] = array(
                'NAME' => $licenseCols->item(1)->textContent,
                'PRICE' => $licenseCols->item(3)->textContent,
                'USERS_COUNT' => !empty($usersCount[1]) ? (int) $usersCount[1] : 0,
                'TYPE' => $type,
                'MICROSOFT' => $microsoft,
                'LICENSE_PRODUCT' => $licensesProductDBId,
            );
        }

        return $licensesList;
    }

    function saveLicensesToDB($licensesList)
    {
        if ($licensesList)
        {
            $arHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::getById($this->licensesListHLBId)->fetch();
            $obEntity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
            $strEntityDataClass = $obEntity->getDataClass();

            foreach ($licensesList as $license)
            {
                $arElementFields = array(
                    'UF_NAME' => mb_convert_encoding($license['NAME'], 'windows-1251', 'utf-8'),
                    'UF_SORT' => 500,
                    'UF_PRICE' => $license['PRICE'],
                    'UF_USERS_COUNT' => $license['USERS_COUNT'],
                    'UF_TYPE' => $license['TYPE'],
                    'UF_MICROSOFT' => $license['MICROSOFT'],
                    'UF_LICENSE_PRODUCT' => $license['LICENSE_PRODUCT'],
                );
                $obResult = $strEntityDataClass::add($arElementFields);
            }
        }

        return $licensesList;
    }

    private function getUrlHtml($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $html = curl_exec($ch);

        return $html;
    }

    private function sendErrorResponse($message)
    {
        echo json_encode(
            array(
                'status' => 'error',
                'message' => $message,
            ),
            256
        );

        exit;
    }

    private function sendSuccessResponse($message, $pcts)
    {
        echo json_encode(
            array(
                'status' => 'success',
                'stepMessage' => $message,
                'stepPcts' => $pcts,
            ),
            256
        );

        exit;
    }

    private function sendFinishedResponse($message)
    {
        echo json_encode(
            array(
                'status' => 'finished',
                'stepMessage' => $message,
            ),
            256
        );

        exit;
    }

    private function writeTempData($nextStep, $data)
    {
        $tempData = json_encode(
            array(
                'nextStep' => $nextStep,
                'data' => $data,
            ),
            256
        );

        \Bitrix\Main\IO\File::putFileContents(
            $this->tempDataFile,
            $tempData
        );
    }

    private function readTempData()
    {
        $tempData = \Bitrix\Main\IO\File::isFileExists($this->tempDataFile) ? json_decode(\Bitrix\Main\IO\File::getFileContents($this->tempDataFile), true) : array();

        return $tempData;
    }

}