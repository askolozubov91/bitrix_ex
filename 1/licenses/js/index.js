$(document).ready(function() {
    var $progress_step = $('.progress-step'),
        $progress_bar__bar = $('.progress-bar__bar'),
        $progress_bar__step = $('.progress-bar__pcts');

    $('.parser-form').on('click', function (e) {
        e.preventDefault();

        $('.progress').show();
        $('.parser-form [type="submit"]').hide();

        nextStep('Y');
    });

    function nextStep(isNew) {
        isNew = typeof isNew == 'undefined' ? 'N' : isNew;

        $.ajax({
            type: 'post',
            url: '/public_services/licenses/?AJAX_MODE=Y&IS_NEW=' + isNew,
            dataType: 'json',
            success:
                function (response) {
                    if (response.status == 'success') {
                        $progress_step.html(response.stepMessage);
                        $progress_bar__bar.css('width', response.stepPcts + '%');
                        $progress_bar__step.html(response.stepPcts + '%');

                        nextStep();
                    }
                    else if (response.status == 'error') {
                        alert('error');
                    }
                    else if (response.status == 'finished') {
                        $progress_step.html(response.stepMessage);
                        $progress_bar__bar.css('width', '100%');
                        $progress_bar__step.html('100%');
                    }
                    else {
                        alert('unexpected answer');
                    }
                },
            error: function (jqXHR, exception) {
                console.log(jqXHR);
                console.log(exception);
            }
        });
    }
});