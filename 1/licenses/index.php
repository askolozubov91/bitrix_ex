<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

global $USER;

if (!$USER->IsAdmin())
{
    echo 'You are not allowed to use this script';

    exit;
}

if (empty($_REQUEST['AJAX_MODE']) || $_REQUEST['AJAX_MODE'] != 'Y'):
?>
    <html>
        <head>
            <script type="text/javascript" src="<?php echo SITE_TEMPLATE_PATH . "/js/jquery-1.8.3.min.js"; ?>"></script>
            <script type="text/javascript" src="/public_services/licenses/js/index.js"></script>
            <link rel="stylesheet" type="text/css" href="/public_services/licenses/css/index.css">
        </head>
        <body>
            <form class="parser-form" name="parser-form">
                <div class="progress">
                    <div class="progress-bar">
                        <div class="progress-bar__bar" style="width: 0%;"></div>
                        <div class="progress-bar__pcts">0%</div>
                    </div>
                    <div class="progress-step">
                        Parsing types
                    </div>
                </div>
                <button type="submit">Start</button>
            </form>
        </body>
    </html>
<?php
else:

$isNew = !empty($_GET['IS_NEW']) && $_GET['IS_NEW'] == 'Y';

$licensesHelper = new LicensesHelper();

if ($isNew)
{
    $licensesHelper->clearPreviousData();
}

$licensesHelper->processStep();

endif;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
