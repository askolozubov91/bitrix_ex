<?php
define('FLAG_SHOW_PROMO_TEMPLATE', 'Y');


require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("�������� 1�");

if ($_SESSION['REGION']['COUNTRY_CODE'] != 'ru')
{
    SetCurPage404("LITE");
}

$APPLICATION->SetPageProperty('title', '�������� 1� ����������� 8 � ������ � #CITY_DATIVE#');
$APPLICATION->SetPageProperty('description', '&#10003; �������� 1� ����������� 8 � ����, �����������, ������������, ������ � ������� �������� 1�');
$APPLICATION->SetTitle('�������� 1�: ����������� 8');

define('IB_SECTION_LICENSES', 'litsenzii-1s-predpriyatie-8-66');

\Bitrix\Main\Page\Asset::getInstance()->addCss(MAIN_TEMPLATE_PATH . "/vendor/OwlCarousel2/dist/assets/owl.carousel.min.css");
\Bitrix\Main\Page\Asset::getInstance()->addCss(MAIN_TEMPLATE_PATH . '/js/webui-popover/src/jquery.webui-popover.css');
\Bitrix\Main\Page\Asset::getInstance()->addCss(Bitrix\Main\Context::getCurrent()->getRequest()->getRequestedPageDirectory() . "/css/select2.min.css");
\Bitrix\Main\Page\Asset::getInstance()->addCss(Bitrix\Main\Context::getCurrent()->getRequest()->getRequestedPageDirectory() . "/css/index.css");
\Bitrix\Main\Page\Asset::getInstance()->addJs(Bitrix\Main\Context::getCurrent()->getRequest()->getRequestedPageDirectory() . "/js/index.js", true);

$arLicensesFilter = FilterHelper::make(
    array(
        'IBLOCK_ID' => IB_CATALOG,
        'SECTION_CODE' => IB_SECTION_LICENSES,
        '!PROPERTY_LICENSE_TYPE' => false,
    ),
    array(
        'IS_CITY' => true,
    )
);
$cacheTime = 3600000;
$cacheId = serialize($arLicensesFilter);
$cacheDir = 'catalogue_licenses';
$cache = \Bitrix\Main\Data\Cache::createInstance();

if ($cache->initCache($cacheTime, $cacheId, $cacheDir))
{
    $vars = $cache->getVars();
    $arLicenses = $vars['arLicenses'];
    $arLicensesTypes = $vars['arLicensesTypes'];
    $arLicensesProducts = $vars['arLicensesProducts'];
}
elseif ($cache->startDataCache())
{
    $rsLicenseTypes = CIBlockPropertyEnum::GetList(
        array(
            "SORT" => "ASC"
        ),
        array(
            "IBLOCK_ID" => IB_CATALOG,
            "CODE" => "LICENSE_TYPE",
        )
    );
    $arLicenseTypes = array();
    while ($arLicenseType = $rsLicenseTypes->GetNext()) {
        $arLicenseTypes[$arLicenseType['ID']] = $arLicenseType['XML_ID'];
    }

    $rsLicenses = CIblockElement::GetList(
        array(
            'SORT' => 'ASC',
        ),
        $arLicensesFilter,
        false,
        false,
        array(
            'IBLOCK_ID',
            'ID',
            'NAME',
            'PROPERTY_hours',
            'PROPERTY_LICENSE_TYPE',
            'PROPERTY_RUB',
        )
    );
    $arLicenses = array();
    while ($arLicense = $rsLicenses->GetNext()) {
        $arLicenses[$arLicenseTypes[$arLicense['PROPERTY_LICENSE_TYPE_ENUM_ID']]][] = $arLicense;
    }

    CModule::IncludeModule('highloadblock');

    $arHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::getById(HLB_CATALOGUE_LICENSES_TYPES)->fetch();
    $obEntity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
    $licensesTypesClass = $obEntity->getDataClass();

    $arLicensesTypes = array();
    $rsLicensesTypes = $licensesTypesClass::getList(
        array(
            'select' => array('ID', 'UF_NAME'),
            'order' => array('ID' => 'ASC'),
        )
    );
    while ($arLicensesType = $rsLicensesTypes->Fetch()) {
        $arLicensesTypes[$arLicensesType['ID']] = $arLicensesType;
    }

    $arHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::getById(HLB_CATALOGUE_LICENSES_PRODUCTS)->fetch();
    $obEntity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
    $licensesProductsClass = $obEntity->getDataClass();

    $arLicensesProducts = array();
    $rsLicensesProducts = $licensesProductsClass::getList(
        array(
            'select' => array('ID', 'UF_NAME', 'UF_PRICE', 'UF_LICENSE_TYPE'),
            'order' => array('UF_LICENSE_TYPE' => 'ASC', 'ID' => 'ASC'),
        )
    );
    while ($arLicensesProduct = $rsLicensesProducts->Fetch()) {
        $arLicensesProducts[$arLicensesProduct['UF_LICENSE_TYPE']][$arLicensesProduct['ID']] = $arLicensesProduct;
    }

    $cache->endDataCache(
        array(
            'arLicenses' => $arLicenses,
            'arLicensesTypes' => $arLicensesTypes,
            'arLicensesProducts' => $arLicensesProducts,
        )
    );
}

$arLicensesParams = array(
    array(
        'XML_ID' => 'license_type_single',
        'TITLE' => '�������������������� ���������� ��������',
        'SUBTITLE' => '��� ������� ������������� ���������� ���������� ���������� �� ����� ����������',
        'SUFFIX' => 'single',
    ),
    array(
        'XML_ID' => 'license_type_multiple',
        'TITLE' => '��������������������� ���������� �������� ',
        'SUBTITLE' => '��� ������� ������������� ���������� ���������� ���������� � ������������ �����������. ���������� ������������ ���������� ���������� ���������� ������������ ��������� ��������',
        'SUFFIX' => 'multi',
    ),
    array(
        'XML_ID' => 'license_type_corp',
        'TITLE' => '�������� ����',
        'SUBTITLE' => '��� ������� ������������� ��������',
        'SUFFIX' => 'corp',
    ),
    array(
        'XML_ID' => 'license_type_server32',
        'TITLE' => '��������� �������� �� 32-��������� ������',
        'SUBTITLE' => '��� ������������� ������������� ���������� 32-��������� ������� ��������� �� ����� ����������',
        'SUFFIX' => 'server-32',
    ),
    array(
        'XML_ID' => 'license_type_server64',
        'TITLE' => '��������� �������� �� 64-��������� ������',
        'SUBTITLE' => '��� ������������� ������������� ���������� 32-��������� ��� 64-��������� ������� ��������� �� ����� ����������',
        'SUFFIX' => 'server-64',
    ),
);
?>

<div class="c-wrap">

	<div class="c-header">
		<div class="c-container">
			<h1 class="c-header__title">�������� 1�: ����������� 8</h1>
			<div class="c-header__subtitle">��� ���������� ������ �������������<br> � ���������� 1�: ����������� 8, 8.1, 8.2, 8.3.</div>
		</div>
	</div>


	<div class="c-menu">
        <div class="c-container">
            <ul class="c-menu__list">
                <li class="c-menu__item"><a href="#license-types" class="c-menu__link">���� �������������� </a></li>
                <?php if (!empty($_GET['SHOW_CALC']) && $_GET['SHOW_CALC'] == 'Y'): ?>
                    <li class="c-menu__item"><a href="#price-calculator" class="c-menu__link">����������� ��������� </a></li>
                <?php endif; ?>
                <li class="c-menu__item"><a href="#consultation" class="c-menu__link">������������ �������� </a></li>
                <li class="c-menu__item"><a href="#price-list" class="c-menu__link">�����-����</a></li>
            </ul>
        </div>
	</div>


	<div class="c-license">
		<div class="c-container">
			<div class="c-title">������ �������� 1� ��� �������� ������ ��������</div>

			<div class="c-license__items">
                <?php foreach ($arLicensesParams as $arLicenseParams): ?>
                    <?php if (!empty($arLicenses[$arLicenseParams['XML_ID']])): ?>
                        <div class="c-license__item c-license__item_<?php echo $arLicenseParams['SUFFIX']; ?>">
                            <div class="c-license__item-content">
                                <div class="c-license__item-title"><?php echo $arLicenseParams['TITLE']; ?></div>
                                <div class="c-license__item-subtitle"><?php echo $arLicenseParams['SUBTITLE']; ?>
                                </div>
                            </div>
                            <a class="c-license__item-link" href="#licence-<?php echo $arLicenseParams['SUFFIX']; ?>">��������</a>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
			</div>
		</div>
	</div>


	<div class="c-types" id="license-types">
		<div class="c-container">
			<div class="c-title">���� �������������� </div>

			<div class="c-types__items owl-carousel j-owl-carousel" data-dots-speed="700">
				<div class="c-types__item">
					<div class="c-types__item-wrap">
						<div class="c-types__item-left">
							<img src="img/slide_1_new.jpg" alt="">
						</div>
						<div class="c-types__item-right">
							<div class="c-types__item-title">����������� ��������</div>
							<div class="c-types__item-text">
								<p>��������������� ����� �������� �������� � ����������, ���� ����� ����������� ��������� 1�.</p>
								<p>������������ �������� ���-���� ���������.</p>
							</div>
						</div>
					</div>
				</div>

				<div class="c-types__item">
					<div class="c-types__item-wrap">
						<div class="c-types__item-left">
							<img src="img/slide_2_new.jpg" alt="">
						</div>
						<div class="c-types__item-right">
							<div class="c-types__item-title">���������� �������� </div>
							<div class="c-types__item-text">
								<p>��� USB-����, ������� ��������� ���������
									�������� ����� ������ �������.</p>
								<p>��������� ������ �������� �� ��������� ���������� ������������ � ���������� ��������� 1�.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="c-calculator" id="price-calculator">
		<div class="c-container">

            <?php if (!empty($_GET['SHOW_CALC']) && $_GET['SHOW_CALC'] == 'Y'): ?>
                <div class="c-calculator__content">
                    <div class="c-title">����������� ���������</div>
                    <p class="c-calculator__subtitle">����������� ����������� ��� ������� ��������� ������������ ��������������</p>

                    <form class="c-calculator__wrap" action="#" id="calculator-form">
                        <div class="c-calculator__field">
                            <div class="c-calculator__label">��� ��������� ������������ ��������:</div>
                            <select class="c-calculator__select js-custom-select j-select" id="licenses-type" name="licensesType">
                                <option value="0">��� ����</option>
                                <?php foreach($arLicensesTypes as $arLicensesType): ?>
                                    <option value="<?php echo $arLicensesType['ID']; ?>"><?php echo $arLicensesType['UF_NAME']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="c-calculator__field">
                            <div class="c-calculator__label">�������� ����������� �������: </div>
                            <select class="c-calculator__select js-custom-select j-select" id="licenses-product" name="licensesProduct">
                                <option value="0">�������� �������� �������</option>
                                <?php foreach($arLicensesProducts as $arLicensesTypeId => $arLicensesProductsList): ?>
                                    <optgroup label="<?php echo $arLicensesTypes[$arLicensesTypeId]['UF_NAME']; ?>" data-LICENSE_TYPE="<?php echo $arLicensesTypeId; ?>"></optgroup>
                                    <?php foreach($arLicensesProductsList as $arLicensesProduct): ?>
                                        <option value="<?php echo $arLicensesProduct['ID']; ?>" data-PRICE="<?php echo $arLicensesProduct['UF_PRICE']; ?>" data-LICENSE_TYPE="<?php echo $arLicensesTypeId; ?>"><?php echo $arLicensesProduct['UF_NAME']; ?></option>
                                    <?php endforeach; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <label class="g-checkbox" for="check1">
                            <input class="g-checkbox-input" id="check1" type="checkbox" name="microsoft">
                            <span class="g-checkbox-icon g-icon-ok"></span>
                            <span class="c-calculator__text-light">���������� �������� 1� � Microsoft ������� �MS SQL Server ��� 1�:����������� 8�</span>
                        </label>

                        <div class="c-calculator__field">
                            <div class="c-calculator__label">������� ������:</div>
                            <p>��� ������� �������� ��� ������-���������� �������� ������ ����������� ����������� ������ � �������� ��������. ���
                                ������ � �������� �������-������ ��������� ������ ��� ������ Microsoft SQL Server 2000 ��� ����. </p>
                        </div>

                        <div>
                            <label class="g-radio" for="radio1">
                                <input class="g-radio-input" id="radio1" type="radio" name="type" value="file" checked>
                                <span class="g-radio-icon"></span>
                                <span>��������</span>
                            </label>

                            <label class="g-radio" for="radio2">
                                <input class="g-radio-input" id="radio2" type="radio" name="type" value="server">
                                <span class="g-radio-icon"></span>
                                <span>������-������</span>
                            </label>
                        </div>

                        <p class="c-calculator__text-light">��� ��������������� ������� ������������ ��������� ���������� �������� ������� ����� ���������� � ������ �����������
                            ������������� �������� � ������� ���������� �������� </p>

                        <div class="c-calculator__license">
                            <span class="c-calculator__license-label">���������� ������������� ��������:</span>
                            <input type="text" class="c-calculator__input-text" placeholder="0" name="licenses_needed">
                            <button type="button" class="c-btn c-smart-calc-btn">��������� ��������</button>
                        </div>
                    </form>

                    <div class="c-calculator__wrap">
                        <div class="c-calculator-table c-calculator-table__type-server" style="display: none;">
                            <div class="c-calculator-table__header">��� ������ � �������� �������-������: </div>
                            <div class="c-calculator-table__head">
                                <div class="c-calculator-table__row">
                                    <div class="c-calculator-table__row-item">������������</div>
                                    <div class="c-calculator-table__row-item">����������</div>
                                    <div class="c-calculator-table__row-item">���������, ���.</div>
                                    <div class="c-calculator-table__row-item">�����,����.</div>
                                </div>
                            </div>
                            <div class="c-calculator-table__body">
                            </div>
                        </div>
                        <div class="c-calculator-table c-calculator-table__type-file" style="display: none;">
                            <div class="c-calculator-table__header">��� ������ � �������� ���������: </div>
                            <div class="c-calculator-table__head">
                                <div class="c-calculator-table__row">
                                    <div class="c-calculator-table__row-item">������������</div>
                                    <div class="c-calculator-table__row-item">����������</div>
                                    <div class="c-calculator-table__row-item">���������, ���.</div>
                                    <div class="c-calculator-table__row-item">�����,����.</div>
                                </div>
                            </div>
                            <div class="c-calculator-table__body">
                            </div>
                        </div>
                        <div class="c-calculator-table c-calculator-table__total" style="display: none;">
                            <div class="c-calculator-table__header">������������� ����������� ��������</div>
                            <div class="c-calculator-table__head">
                                <div class="c-calculator-table__row">
                                    <div class="c-calculator-table__row-item">������������</div>
                                    <div class="c-calculator-table__row-item">����������</div>
                                    <div class="c-calculator-table__row-item">���������, ���.</div>
                                    <div class="c-calculator-table__row-item">�����,����.</div>
                                </div>
                            </div>
                            <div class="c-calculator-table__body">
                            </div>
                        </div>
                        <div class="c-calculator__table-bottom">
                            <div class="c-calculator__table-bottom-left">
                                <span class="c-calculator__table-sum-label">�������� �����: </span>
                                <span class="c-calculator__table-sum-value"><span class="c-calculator__table-sum-value-num">0</span> ������</span>
                            </div>
                            <div class="c-calculator__table-bottom-right">
                                <a href="#" class="c-btn 1csoft-order-btn">��������</a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

			<div class="c-license-full" id="price-list">
                <div class="c-title">�����-����</div>
                <?php foreach ($arLicensesParams as $arLicenseParams): ?>
                    <?php if (!empty($arLicenses[$arLicenseParams['XML_ID']])): ?>
                        <div class="c-license-full__item c-license-full__item_<?php echo $arLicenseParams['SUFFIX']; ?>" id="licence-<?php echo $arLicenseParams['SUFFIX']; ?>">
                            <div class="c-license-full__item-head"><?php echo $arLicenseParams['TITLE']; ?></div>
                            <div class="c-license-full__item-content">
                                <div class="c-calculator-table">
                                    <div class="c-calculator-table__row c-calculator-table__head">
                                        <div class="c-calculator-table__row-item">�������� ������</div>
                                        <div class="c-calculator-table__row-item">
                                            <span>���� � �������</span>
                                            <a href="javascript:void(0)" class="popover-icon c-calculator-table_info-icon" data-content="<!--noindex-->������������ ���������� ����� ������ ����������� �� ��������� 1�, �������� � ��������� ������������ ��������. �� ��������� ����������� ���������� � ���������.<!--/noindex-->" data-placement="right-top"></a>
                                        </div>
                                        <div class="c-calculator-table__row-item">���������, ���.</div>
                                        <div class="c-calculator-table__row-item">������</div>
                                    </div>
                                    <?php foreach ($arLicenses[$arLicenseParams['XML_ID']] as $arLicense): ?>
                                        <div class="c-calculator-table__row">
                                            <div class="c-calculator-table__row-item"><?php echo $arLicense['NAME']; ?></div>
                                            <div class="c-calculator-table__row-item"><?php echo $arLicense['PROPERTY_HOURS_VALUE']; ?></div>
                                            <div class="c-calculator-table__row-item"><?php echo number_format($arLicense['PROPERTY_RUB_VALUE'], 0, '.', ' '); ?></div>
                                            <div class="c-calculator-table__row-item buy-width"><a href="javascript:void(0);" class="btn-order resp-btn c1pp" data-form-title="<?php echo $arLicense['NAME'];?>"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>������</a></div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>

			</div>
		</div>
	</div>


	<!--<div class="c-order" id="consultation">  
		<div class="c-container">
			<a id="1csoft-order-btn" href="#" class="c-order__btn c-order__btn_pink 1csoft-order-btn">�������� �����</a>
			<span class="c-order__text">���</span>
			<a id="general-form-106-btn" href="#" class="c-order__btn c-order__btn_border">�������� ������������</a>
		</div>
	</div>-->


	<div class="c-feedback">
		<div class="c-container">
			<div class="c-feedback__wrap">
				<div class="c-title">� ��� ���� ������� ��� ������ ��� ���?</div>
				<div class="c-feedback__subtitle">������� �������. ���������. � �������!</div>
                <?php
                $APPLICATION->IncludeComponent(
                    "bezr:form.result.new.befsend",
                    "inner_line_consult_1csoft",
                    array(
                        "WEB_FORM_ID" => WEB_FORM_CONSULT_REQUEST_FEEDCALL,
                        "COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
                        "COMPANY_IBLOCK_ID" => IB_CONTACTS,
                        "PRODUCT_IBLOCK_TYPE" => IBT_CATALOG,
                        "PRODUCT_IBLOCK_ID" => IB_CATALOG,
                        "CITY_QUESTION_CODE" => "CITY",
                        "OFFICE_QUESTION_CODE" => "OFFICE",
                        "TO_QUESTION_CODE" => "TO",
                        "IGNORE_CUSTOM_TEMPLATE" => "Y",
                        "USE_EXTENDED_ERRORS" => "Y",
                        "CACHE_TYPE" => 'A',
                        "CACHE_TIME" => '36000000',
                        "LIST_URL" => "",
                        "EDIT_URL" => "",
                        "SUCCESS_URL" => "",
                        "CHAIN_ITEM_TEXT" => "",
                        "CHAIN_ITEM_LINK" => "",
                        "VARIABLE_ALIASES" => array(
                            "WEB_FORM_ID" => "WEB_FORM_ID",
                            "RESULT_ID" => "RESULT_ID",
                        ),
                         "SHOW_FORM_DESCRIPTION" => "N"
                    ),
                    false,
                    Array("HIDE_ICONS" => "Y")
                );
                ?>
			</div>
		</div>
	</div>

</div>

<?php
$APPLICATION->IncludeComponent(
    "bezr:form.result.new.befsend",
    "catalog_order",
    array(
        "WEB_FORM_ID" => WEB_FORM_BUY_PRODUCT,
        "COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
        "COMPANY_IBLOCK_ID" => IB_CONTACTS,
        "PRODUCT_IBLOCK_TYPE" => IBT_CATALOG,
        "PRODUCT_IBLOCK_ID" => IB_CATALOG,
        "CITY_QUESTION_CODE" => "CITY",
        "OFFICE_QUESTION_CODE" => "OFFICE",
        "PRODUCT_QUESTION_CODE" => "PRODUCT",
        "TO_QUESTION_CODE" => "TO",
        "TO_QUESTION_RESIPIENTS" => '',
        "EMAIL_ONLY_FROM_RESIPIENTS" => "N",
        "SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
        "EMAIL_QUESTION_CODE" => "EMAIL",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "USE_EXTENDED_ERRORS" => "Y",
        "CACHE_TYPE" => 'A',
        "CACHE_TIME" => 36000000,
        "LIST_URL" => "",
        "EDIT_URL" => "",
        "SUCCESS_URL" => "",
        "CHAIN_ITEM_TEXT" => "",
        "CHAIN_ITEM_LINK" => "",
        "VARIABLE_ALIASES" => array(
            "WEB_FORM_ID" => "WEB_FORM_ID",
            "RESULT_ID" => "RESULT_ID",
        ),
        "PRODUCT_SECTION" => '',
        "PRODUCT_SECTION_CODE" => 'litsenzii-1s-predpriyatie-8-66',
        "BASE_PRODUCT_NAME" => '�������� 1�: ����������� 8',
        "BTN_CALL_FORM" => '.1csoft-order-btn, .btn-order',
        "SELECTOR_PRODUCT_TITLE" => '.popup-title span',
        "SELECTOR_PRODUCT_MODULE_BOX" => '#prices .table-present, #products .table-present, .price-list .pricelist-table',
        "SELECTOR_PRODUCT_CHANGE_LINK" => '#product-order-list .j-new-choose',
        "COMPONENT_MARKER" => "product-order"
    ),
    false,
    Array("HIDE_ICONS"=>"Y")
);
?>

<?php
$APPLICATION->IncludeComponent(
    "bezr:form.result.new.befsend",
    "general",
    array(
        "WEB_FORM_ID" => WEB_FORM_CALLBACK_REGIONS,
        "COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
        "COMPANY_IBLOCK_ID" => IB_CONTACTS,
        "PRODUCT_IBLOCK_TYPE" => IBT_CATALOG,
        "PRODUCT_IBLOCK_ID" => IB_CATALOG,
        "CITY_QUESTION_CODE" => "CITY",
        "OFFICE_QUESTION_CODE" => "OFFICE",
        "PRODUCT_QUESTION_CODE" => "PRODUCT",
        "TO_QUESTION_CODE" => "TO",
        "TO_QUESTION_RESIPIENTS" => '',
        "EMAIL_ONLY_FROM_RESIPIENTS" => "N",
        "SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
        "EMAIL_QUESTION_CODE" => "EMAIL",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "USE_EXTENDED_ERRORS" => "Y",
        "CACHE_TYPE" => 'A',
        "CACHE_TIME" => 36000000,
        "LIST_URL" => "",
        "EDIT_URL" => "",
        "SUCCESS_URL" => "",
        "CHAIN_ITEM_TEXT" => "",
        "CHAIN_ITEM_LINK" => "",
        "VARIABLE_ALIASES" => array(
            "WEB_FORM_ID" => "WEB_FORM_ID",
            "RESULT_ID" => "RESULT_ID",
        ),
        "BTN_CALL_FORM" => "#general-form-106-btn"
    ),
    false,
    Array("HIDE_ICONS"=>"Y")
);
?>

<?php require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
