(function(){

//====================
// select2 options
//====================

//====================
// smooth scrolling to
//====================

function smoothScrollTo(linkEl) {
    elementClick = linkEl.attr("href");
    destination = $(elementClick).offset().top;
    $('body,html').animate( { scrollTop: destination - 70}, 800 );
    return false;
}
	
$('a[href^="#"].c-menu__link').click(function () { 
    smoothScrollTo($(this));
});


//===================
// accordion + links
//===================

$('.c-license-full__item').eq(0).addClass('active').find('.c-license-full__item-content').slideDown();


(function() {
    var o, n;
    $(".c-license-full__item-head").on("click", function() {
      o = $(this).parents(".c-license-full__item");
      n = o.find(".c-license-full__item-content"),
        o.hasClass("active") ? (o.removeClass("active"),
          n.slideUp()) : (o.addClass("active"), n.stop(!0, !0).slideDown(),
          o.siblings(".active").removeClass("active").children(
            ".c-license-full__item-content").stop(!0, !0).slideUp())
    });
})();

(function() {
    require(['Controls/Responsive'], function (obResponsive) {
        obResponsive.one(['small','medium','large'], 'in', function () {
            $('.c-license__item-link').on('click', function() {
                var $this = $(this);
                var curHref = $(this).attr('href');
                var targetEl = $(curHref);
                if (!targetEl.closest(".c-license-full__item").hasClass('active')) {
                    targetEl.closest(".c-license-full__item").addClass('active');

                    targetEl.closest(".c-license-full__item").siblings().each(function() {
                        if ($(this).hasClass('active')) {
                            $(this).removeClass('active');
                            $(this).find(".c-license-full__item-content").css('display', 'none');
                        }
                    });

                    targetEl.closest(".c-license-full__item").find(".c-license-full__item-content").css('display', 'block');
                }
                smoothScrollTo($this);
            });
        });
    });
})();

    //===================
    // slider
    //===================

    $('.c-order__btn').on('click', function(e) {
        e.preventDefault();
    });

    //===================
    // calculator
    //===================

    var $calculator_form = $('#calculator-form'),
        $server_table = $('.c-calculator-table__type-server'),
        $server_table_body = $server_table.find('.c-calculator-table__body'),
        $file_table = $('.c-calculator-table__type-file'),
        $file_table_body = $file_table.find('.c-calculator-table__body'),
        $total_table = $('.c-calculator-table__total'),
        $total_table_body = $total_table.find('.c-calculator-table__body');

    $calculator_form.on('change', '[name="licensesType"]', function(){
        var $license_type_input = $(this);

        if ($license_type_input.val() == 0)
        {
            $calculator_form
                .find('#cusel-scroll-licenses-product')
                    .children()
                        .show();
        }
        else
        {
            $calculator_form
                .find('#cusel-scroll-licenses-product')
                    .children()
                        .hide()
                        .filter('[val="0"], [data-license_type="' + $license_type_input.val() + '"]')
                            .show();
        }

        $calculator_form
            .find('#cusel-scroll-licenses-product')
                .children()
                    .first()
                        .click();

        var params = {
            refreshEl: "#licenses-product",
            visRows: 6,
            scrollArrows: true
        };
        cuSelRefresh(params);
    });

    $calculator_form.on('change', '[name="licensesProduct"], [name="type"], [name="microsoft"]', function() {
        getLicenses();
        calculateCost();
    });

    $('.c-calculator').on('change', '.licenses-count', function() {
        calculateCost();
    });

    $calculator_form.find('.c-smart-calc-btn').on('click', function() {
        smartSelect();
    });

    function getLicenses() {
        $server_table_body.html('');
        $file_table_body.html('');
        $server_table.hide();
        $file_table.hide();

        var form_data = new FormData(),
            licenseProduct = $calculator_form.find('[name="licensesProduct"]').val();

        if (licenseProduct == 0)
        {
            return false;
        }

        form_data.append('licensesProduct', licenseProduct);
        form_data.append('microsoft', $calculator_form.find('[name="microsoft"]').is(':checked') ? 1 : 0);
        form_data.append('type', $calculator_form.find('[name="type"]:checked').val());

        $.ajax({
            type: 'post',
            url: '/1csoft/litsenzii-1s-predpriyatie/ajax.php',
            data: form_data,
            dataType: 'json',
            processData: false,
            contentType: false,
            success:
                function (response) {
                console.log(response);
                    if (response.status == 'success') {
                        if (response.data.serverTableHtml != '')
                        {
                            $server_table_body.html(response.data.serverTableHtml);
                            $server_table.show();
                        }
                        else
                        {
                            $server_table.hide();
                        }

                        if (response.data.fileTableHtml != '')
                        {
                            $file_table_body.html(response.data.fileTableHtml);
                            $file_table.show();
                        }
                        else
                        {
                            $file_table.hide();
                        }
                    }
                    else if (response.status == 'error') {
                        alert('error');
                    }
                    else {
                        alert('unexpected answer');
                    }
                },
            error: function (jqXHR, exception) {
                console.log(jqXHR);
                console.log(exception);
            }
        });
    }

    function calculateCost() {
        var $table_sum_value = $('.c-calculator__table-sum-value-num'),
            $license_product_input = $calculator_form.find('[name="licensesProduct"]');

        if ($license_product_input.val() == 0)
        {
            $table_sum_value.html(0);
            $total_table.hide();

            return false;
        }

        var $licenses_product = $('#cusel-scroll-licenses-product .cuselActive'),
            $licenses_count = $('.licenses-count'),
            cost = 0;

        cost += $licenses_product.data('price');

        $total_table_body.html('\
            <div class="c-calculator-table__row">\
                <div class="c-calculator-table__row-item">' + $licenses_product.html() + '</div>\
                <div class="c-calculator-table__row-item">1</div>\
                <div class="c-calculator-table__row-item">1</div>\
                <div class="c-calculator-table__row-item">' + $licenses_product.data('price') + '</div>\
            </div>\
        ');

        $total_table.show();

        for (var i = 0; i < $licenses_count.length; i++)
        {
            var $input = $licenses_count.eq(i),
                $item_row = $input.closest('.c-calculator-table__row'),
                count = $input.val();

            if (count > 0)
            {
                var price = $item_row.data('price'),
                    item_cost = count * price;

                cost += item_cost;
                $item_row
                    .addClass('c-calculator-table__row_selected')
                    .find('.c-calculator-table__row-item-total')
                        .html(item_cost);

                $total_table_body.append('\
                    <div class="c-calculator-table__row">\
                        <div class="c-calculator-table__row-item">' + $item_row.find('.c-calculator-table__row-item-title').html() + '</div>\
                        <div class="c-calculator-table__row-item">' + count + '</div>\
                        <div class="c-calculator-table__row-item">' + price + '</div>\
                        <div class="c-calculator-table__row-item">' + item_cost + '</div>\
                    </div>\
                ');
            }
            else
            {
                $item_row
                    .removeClass('c-calculator-table__row_selected')
                    .find('.c-calculator-table__row-item-total')
                        .html(0);
            }
        }

        $table_sum_value.html(cost);
    }

    function smartSelect() {
        if ($file_table_body.html() == '')
        {
            return false;
        }

        var $licenses_rows = $file_table_body.find('.c-calculator-table__row'),
            licenses_needed = $calculator_form.find('[name="licenses_needed"]').val();

        if (licenses_needed < 0)
        {
            return false;
        }

        $file_table_body.find('.licenses-count').val(0);

        for (var i = $licenses_rows.length - 1; i >= 0; i--)
        {
            var $licenses_row = $licenses_rows.eq(i),
                users_count = $licenses_row.data('users_count');

            if (!users_count || users_count > licenses_needed)
            {
                continue;
            }

            var licenses_count = Math.floor(licenses_needed/users_count);

            licenses_needed -= licenses_count * users_count;

            $licenses_row.find('.licenses-count').val(licenses_count);

            if (licenses_needed == 0)
            {
                break;
            }
        }

        calculateCost();
    }

})();

$(function () {
    require(['popover'], function () {
        $('.popover-icon').webuiPopover({trigger: 'hover'});
    });
});