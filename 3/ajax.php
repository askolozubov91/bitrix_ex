<?php
header('Content-type: text/html; charset=utf-8');
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$licensesProduct = !empty($_REQUEST['licensesProduct']) ? (int) $_REQUEST['licensesProduct']: 0;
$type = !empty($_REQUEST['type']) ? filter_var($_REQUEST['type'], FILTER_SANITIZE_STRING): 'file';
$microsoft = !empty($_REQUEST['microsoft']) ? (bool) $_REQUEST['microsoft']: false;

if ($licensesProduct)
{
    $arLicensesFilter = array(
        'UF_MICROSOFT' => $microsoft,
        'UF_LICENSE_PRODUCT' => $licensesProduct,
    );

    if ($type == 'file')
    {
        $arLicensesFilter['UF_TYPE'] = 'file';
    }

    $cacheTime = 3600000;
    $cacheId = serialize($arLicensesFilter);
    $cacheDir = 'catalogue_licenses';
    $cache = \Bitrix\Main\Data\Cache::createInstance();
    \Bitrix\Main\Data\Cache::clearCache(true, $cacheDir);
    if ($cache->initCache($cacheTime, $cacheId, $cacheDir))
    {
        $vars = $cache->getVars();
        $fileTableHtml = $vars['fileTableHtml'];
        $serverTableHtml = $vars['serverTableHtml'];
    }
    elseif ($cache->startDataCache())
    {
        CModule::IncludeModule('highloadblock');

        $arHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::getById(HLB_CATALOGUE_LICENSES_LIST)->fetch();
        $obEntity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
        $licensesListClass = $obEntity->getDataClass();

        $arLicenses = array();
        $rsLicenses = $licensesListClass::getList(
            array(
                'select' => array('ID', 'UF_NAME', 'UF_PRICE', 'UF_USERS_COUNT', 'UF_TYPE', 'UF_MICROSOFT', 'UF_LICENSE_PRODUCT'),
                'filter' => $arLicensesFilter,
                'order' => array('UF_USERS_COUNT' => 'ASC', 'UF_SORT' => 'ASC', 'ID' => 'ASC'),
            )
        );

        $fileTableHtml = '';
        $serverTableHtml = '';

        while ($arLicense = $rsLicenses->Fetch()) {
            $html =
                '<div class="c-calculator-table__row" data-ID="' . $arLicense['UF_NAME'] . '" data-USERS_COUNT="' . $arLicense['UF_USERS_COUNT'] . '" data-PRICE="' . $arLicense['UF_PRICE'] . '">
                    <div class="c-calculator-table__row-item c-calculator-table__row-item-title">' . $arLicense['UF_NAME'] . '</div>
                    <div class="c-calculator-table__row-item"><input class="licenses-count" type="text" value="0" name="licenses' . $arLicense['ID'] . '-count"></div>
                    <div class="c-calculator-table__row-item">' . $arLicense['UF_PRICE'] . '</div>
                    <div class="c-calculator-table__row-item c-calculator-table__row-item-total">0</div>
                </div>';

            if ($arLicense['UF_TYPE'] == 'file')
            {
                $fileTableHtml .= $html;
            }
            else
            {
                $serverTableHtml .= $html;
            }
        }

        $cache->endDataCache(
            array(
                'fileTableHtml' => $fileTableHtml,
                'serverTableHtml' => $serverTableHtml,
            )
        );
    }

    echo \Bitrix\Main\Web\Json::encode(
        array(
            'status' => 'success',
            'data' => array(
                'fileTableHtml' => $fileTableHtml,
                'serverTableHtml' => $serverTableHtml,
            ),
        )
    );
    exit;
}
else
{
    echo \Bitrix\Main\Web\Json::encode(
        array(
            'status' => 'error',
            'message' => 'Wrong licensesProduct ID',
        )
    );
    exit;
}
